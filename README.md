# VassalModule-AWorldatWar

A World at War module for Vassal.

This module only supports Clasic AWAW. At some point I want to extend it to also include the elements needed to also support games starting from a Gathering Storm and/or Storm Over Asia.

## Highlights

* High quality maps and counters. From the original game designer. (Thanks Bruce!!!)	
* Full SoP with automatic activation of Oil related actions.
* Feature rich units:
	* Show your units selectivelly by category (Air, Armor and others) to help you to plan your offensives.
	* Hidden Task Force charts to organize your fleet.
	* Uninvert all our naval and air units, taking into account if they redeployed. Also individually.
	* Send your units to the forcepool, normal or isolated, with a click.
	* Air units show their range when on map and according to the map they are.
	* Armor units show their ZOC when on map.
* Oil System:
	* Oil Sources:
		* Oil Sources generate Oil Counters atomatically at SOP phase D.2.a and manually.
		* Unlimited Oil Sources produce more Oil Counters when you take the oneas already there. (Caveats, yous must drop the near the unlimited oil source for that to work)
	* Oil Reserves:
		* On board Oil Reserves to have an easy control of your oil status.
		* Oil Reserves automatically cap to their capacity at the en of your turn.
	* Oil Counters:
		* Oil counter can be moved, consumed on board or added to your reserves.
		* Oil Counters are all destroyed at the end of the turn but the ones in the US Boxes.
		* For convenience, all oil is produced and cosumed multiplied by 5, so it’s directly partial oil counters.
		* Oil system can be hidden to avoid cluttering the board
* Unit counter	
	* Compare your current board forces with the expected force level you must have
	* Find that pesky unit that become lost in Russia
* Free and open	
	* You will find the full module available at [Codeberg](https://codeberg.org/GreyKraken/VassalModule-AWorldatWar)
	* Feel free to post a PR to improve it


## How to build the vmod file

### In Linux (probaly OSX to, but I can't test)

In the root folder of the repository and run
```
scripts/pack
```
This will build the file awawmod.vmod.
You can replace awawmod.vmod by the name you want to give to the mod file

### Windows

Build a zip file with any available tool invluding the files buldFile and moduledata and the complete directory images. Then rename the file to the name you want with the extension vmod.

## How to collaborate with the project

Using the module is a good way to collaborate :)

Do not hesitate to open an issue for any problem you encounter in the module or for any improvement idea.

You can use the usual git workflow and create a PR with any change you want.

## Editing the module

As you will not be editing the files directly, the precedure is rather indirect.

- You will have to build the vmod file in order to edit and test it.
- Edit it with vassal, or any other tool you prefer.
- When ready to commit your changes, run the script scripts/unpack.
- After that you can commit as usual.

# Other tools for playing AWAW

For storing you game data I have also built [AWAW Sheets](https://codeberg.org/JavierVilarroig/AwawSheets)


